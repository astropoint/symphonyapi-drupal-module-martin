<?php
/**
 * @file
 * Template file for the symphony spotlighted event display.
 *
 * Variables available:
 * $event: The event array (the SOAP response transformed into an array)
 *
 */
?>
<div class='symphony-spotlight-div'>
  <?php if (!empty($event['image_thumb'])): ?>
    <div class='symphony-spotlight-colright'>
      <?php print drupal_render($event['image_thumb']); ?>
    </div>
  <?php endif; ?>
  <div class='symphony-spotlight-colleft'>
    <h3>Spotlight event</h3>
    <h4 class='sym-eventname'>
      <?php print drupal_render($event['event_display_link']); ?>
    </h4>
    <?php if(!empty($event['SpotlightDescription'])): ?>
      <?php print $event['SpotlightDescription']; ?>
    <?php endif; ?>
    <p>
      <?php if (!empty($event['institute_link']) && empty($event['institute_link']['#printed'])): ?>
        <?php print drupal_render($event['institute_link']); ?><br />
      <?php endif; ?>
      <?php if (!empty($event['series_link']) && !$event['series_link']['#printed']): ?>
        <?php print drupal_render($event['series_link']); ?><br />
      <?php endif; ?>
      <?php if (!empty($event['StartDate'])): ?>
        <?php print format_date(strtotime($event['StartDate']), 'custom', 'd/m/Y'); ?><br />
        <?php print format_date(strtotime($event['StartDate']), 'custom', 'H:i'); ?><br />
      <?php endif; ?>
      <?php if (!empty($event['Location'])): ?>
          <?php print $event['Location']; ?>
      <?php elseif (!empty($event['Code_Room'])): ?>
          <?php print $event['Code_Room']; ?>
      <?php endif; ?>
    </p>
  </div>
  <?php if (!empty($event['booking_form_button'])): ?>
    <div class="sym-event-book sym-event-item">
      <?php print drupal_render($event['booking_form_button']); ?>
    </div>
  <?php endif; ?>
  <div style='clear:both;' />
</div>
