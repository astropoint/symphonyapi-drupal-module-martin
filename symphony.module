<?php

require_once('symphonyapi.inc');

// Event description character lengths.
define('RESULT_DESC_CHARS', '150');
define('SPOTLIGHT_DESC_CHARS', '200');

// Symphony module Pager defaults
define('SYM_RESULTS_PER_PAGE', variable_get('symphony_results_per_page', 10));
define('SYM_NUM_PAGE_LINKS', variable_get('symphony_num_page_links', 3));

// Events base path (to the events search block)
define('SYM_EVENTS_BASE_PATH', variable_get('symphony_base_path', 'events'));

/**
 * Implements hook_help().
 */
function symphony_help($path, $arg) {
  switch ($path) {
    case "admin/help#symphony":
      return '' . t("Retrieves information from the symphony API") . '';
      break;
  }
}

/*
 * Implements hook_block_info().
 */
function symphony_block_info() {
  $blocks['symphony_events'] = array(
    // The name that will appear in the block list.
    'info' => t('Symphony Events data'),
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function symphony_block_view($delta = '') {
  global $base_address;

  switch ($delta) {
    case 'symphony_events':
      $block['content'] = "";
      if (!user_access('access content')) {
        return $block;
      }

    $block['title'] = t('Search');

        // Use our custom form.
        $filterForm = drupal_get_form('symphony_api_filter_form');
        $block['content'] = drupal_render($filterForm);

    $results = array();

        // Get query params and process the values for symphony api calls.
        $params = drupal_get_query_parameters();

    $filterstart = false;
    if(array_key_exists('start_date', $params)){
      $start_date = strtotime(str_replace("/", "-",$params['start_date']));
      if($start_date>0  && $start_date!==false){
        $filterstart = true;
        $start_date_search = $start_date;
      }
    }
    if(!$filterstart){
      $start_date = strtotime(date("Y-m-d"));
      $start_date_search = time();
      $filterstart = true;
    }


    $filterend = false;
    $end_date_search = null;
    if(array_key_exists('end_date', $params)){
      $end_date = strtotime(str_replace("/", "-",$params['end_date']));
      if($end_date>0  && $end_date!==false){
        $filterend = true;
        $end_date_search = $end_date;
      }
    }

    $filtername = false;
    $event_name = null;
    if(array_key_exists('event_name', $params)){
      $event_name = $params['event_name'];
      $filtername = true;
    }

    // Get value from settings. If empty, look for value in params. Default All (0).
    $instituteid = variable_get('symphony_default_client_company_id', 0);
    if(empty($instituteid) && array_key_exists('institute_id', $params)) {
      // Use id in params
      $instituteid = $params['institute_id'];
    }

    $series = null;
    if(array_key_exists('series', $params)) {
			// Use id in params
      $series = $params['series'];
		}

    // Centre or Project == Symphony Sub Client Company.
    $subinstituteid = null;
    if(array_key_exists('subinstitute_id', $params)){
      $subinstituteid = $params['subinstitute_id'];
    }

    $filtertype = false;
    $event_type = null;
    if(array_key_exists('type', $params)){
      if(($params['type'])!='0'){
        $event_type = $params['type'];
        $filtertype = true;
      }
    }

    // TODO: refactor to use query params
    // Set array if set from
    if(array_key_exists('aoi', $params)){
      //check to see if it is an array already, if not, put it into one as the only element
      if(isset($profileSearchArray) && is_array($profileSearchArray)){
        $profileSearchArray = $_GET['aoi'];
      }else{
        $profileSearchArray = array($_GET['aoi']);
      }
    }else{
      $profileSearchArray = null;
    }

    // Gather paging metadata from the response.
    $pageNumber = pager_find_page() + 1;  // Symphony uses One-based page indexing

    $results = _symphony_get_results($instituteid, $start_date_search, $end_date_search, $event_type, $event_name, $subinstituteid, $pageNumber, SYM_RESULTS_PER_PAGE, $profileSearchArray, $series);
    $numresults = $results['numresults'];
    $events = $results['events'];

        // Prepare the pager
        $pager = pager_default_initialize($numresults, SYM_RESULTS_PER_PAGE);

    // Render the results table
    $block['content'] .= theme('symphony_results', array('results' => $events));
        $block['content'] .= theme('pager', array('quantity' => SYM_NUM_PAGE_LINKS));
      return $block;
  }
}

function symphony_theme() {

  return array(
    'symphony_results' => array(
      'template' => 'symphony-results',
      'path' => drupal_get_path('module', 'symphony') . '/theme',
    ),
    'symphony_spotlight' => array(
      'arguments' => array('event' => NULL),
      'template' => 'symphony-spotlight',
      'path' => drupal_get_path('module', 'symphony') . '/theme',
    ),
    'symphony_api_filter_form' => array(
      'render element' => 'form',
      'arguments' => array('form' => NULL),
    ),
    'symphony_event_display_page' => array(
      'arguments' => array('event' => NULL),
      'template' => 'symphony-event-display-page',
      'path' => drupal_get_path('module', 'symphony') . '/theme',
    ),
  );
}


/**
 *
 * This function uses the attributes passed in to return the HTML of this shortcode.
 *
 * The shortcode needs to be enabled for filtered HTML/full HTML/plain text in
 * Configuration->Text Formats as per preference.
 * Whilst in there, the filter option should also be moved to the bottom of the list
 * to avoid the filtered html from ignoring any other HTML, especially when sent back from symphony
 */
function symphony_shortcode_symspotlight($attrs, $text) {
  // Filter the attributes
  $attrs = shortcode_attrs(
    array(
      'eventid' => ''
    ),
    $attrs
  );

  $event = _symphony_get_event($attrs['eventid']);
  if (!empty($event)) {
    return theme('symphony_spotlight', array('event' => $event));
  }

  return t("The spotlight shortcode eventid is not set correctly");
}


function symphony_api_filter_form($form = array(), &$form_state) {
  // Get the params so they can be used as default values
  // in the form elements.
  $params = drupal_get_query_parameters();

  $form['#attributes'] = array('class' => array('sym-filterform'));

  // Event name == Symphony Event title
  $form['event_name'] = array(
    '#prefix' => '<div class="columns"><div class="column w50">', // M&M SAS Theming markup
    '#type' => 'textfield',
    '#title' => t('Event Name'),
    '#default_value' => (isset($params['event_name']) ? $params['event_name'] : ''),
    '#maxlength' => 128,
    '#size' => 30,
    '#required' => FALSE,
  );

  // Institutes == Symphony Client Company Accounts
  // Make this field 'hidden' if a default Client Company id set.
  // (NB: We need to put this value in the $form_state to use it to perform
  // limiting behaviour for the other dropdown fields' options.
  // If default ccid is in use we also hide institute_id from $_GET parameters)
  $ccid = variable_get('symphony_default_client_company_id', 0);
  $using_default_ccid = !empty($ccid);

  // If we're not using a default, look in the parameters for a value.
  if(!$using_default_ccid && isset($params['institute_id'])) {
    $ccid = $params['institute_id'];
  }
  $form['institute_id'] = array(
    '#type' => (($using_default_ccid) ? 'hidden' : 'select'),
    '#title' => t('Institute'),
    '#options' => _symphony_get_client_company_options(),
    '#default_value' => $ccid,
    '#ajax' => array(
      'callback' => 'ajax_symphony_update_filter_form_fields_callback',
      'progress' => array(
        'message' => NULL
      ),
    ),
  );

  // Sub Institutes = GetSubClientCompanyList
  // Check the $ccid from the form state then use to filter the #options
  $ccid_filter = !empty($form_state['values']['institute_id']) ? $form_state['values']['institute_id'] : $ccid;
  $form['subinstitute_id'] = array(
    '#type' => 'select',
    '#title' => t('Centre or Project'),
    '#default_value' => (isset($params['subinstitute_id']) ? $params['subinstitute_id'] : '0'),
    '#options' => _symphony_get_sub_client_company_options(array($ccid_filter)),
    '#prefix' => '<div id="sym-subclientcompany-field">',
    '#suffix' => '</div>',
  );

  // Areas of Interest == Symphony Profile types
  // Uses the $ccid_filter from above.
  $form['aoi'] = array(
    '#type' => 'select',
    '#multiple' => FALSE,
    '#title' => t('Area of Interest'),
    '#options' => _symphony_get_profile_types_options(array($ccid_filter)),
    '#default_value' => (isset($params['aoi']) ? $params['aoi'] : ''),
    '#prefix' => '<div id="sym-profile-types-field">',
    // First div closes prefix #sym-profile-types-field (used for ajax field replacement)
    // Second div closes event_name prefix .column.w50 (M&M SAS Theming markup)
    '#suffix' => '</div></div>',
  );

  // Event type == Symphony Event type
  $type_list = array("0" => "All");
  $GetEventTypeList = _symphony_get_event_type_list();
  if(is_array($GetEventTypeList)){
    foreach($GetEventTypeList as $typeEntry){
      $type_list[$typeEntry] = $typeEntry;
    }

    $form['type'] = array(
      '#prefix' => '<div class="column w50 right">',  // M&M SAS Theming markup
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => $type_list,
      '#default_value' => (isset($params['type']) ? $params['type'] : '0'),
    );
  }

  // Requires the date.module
  $form['start_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Date From'),
    '#date_format' => 'Y-m-d',
    '#default_value' => (isset($params['start_date']) ? $params['start_date'] : ''),
    '#date_label_position' => 'within',
    '#date_year_range' => '-3:+3',
    '#datepicker_options' => array(),
    '#attributes' => array(
        'placeholder' => 'YYYY-MM-DD',
    ),
    '#pre_render' => array('symphony_date_prerender'),  // Remove date.module desc
  );
  $form['end_date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Date To'),
    '#date_format' => 'Y-m-d',
    '#default_value' => (isset($params['end_date']) ? $params['end_date'] : ''),
    '#date_label_position' => 'within',
    '#date_year_range' => '-3:+3',
    '#datepicker_options' => array(),
    '#attributes' => array(
        'placeholder' => 'YYYY-MM-DD',
    ),
    '#pre_render' => array('symphony_date_prerender'),  // Remove date.module desc
    // M&M SAS Theming markup (closes .column.w50.right and .columns)
    '#suffix' => '</div></div>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Apply'),
  );

  return $form;
}
function symphony_api_filter_form_validate($form, &$form_state) {
  // Validation logic. For now, not required, symphony filters out bad queries.
}
function symphony_api_filter_form_submit($form, &$form_state) {
  // Get the form state variables, clean out empties and unwanted items.
  $params = array_filter($form_state['values']);
  // dpm($params);  // Debug
  unset($params['submit']);
  unset($params['form_build_id']);
  unset($params['form_token']);
  unset($params['form_id']);
  unset($params['op']);

  // Remove institute_id from params if we're using a default ccid.
  if(!empty(variable_get('symphony_default_client_company_id', NULL))) {
    unset($params['institute_id']);
  }

  // Submit to the current path, using the cleaned params in the GET url.
  drupal_goto(current_path(), array('query' => $params));
}
function symphony_date_prerender($element) {
  // Clean off description added by the date.module
  unset($element['date']['#description']);
  return $element;
}
/**
 * Uses the Drupal AJAX Framework commands which do stuff.
 *
 * @see  https://www.drupal.org/node/752056 Drupal ajax api
 * @see  http://drupal.stackexchange.com/a/8530 Replace multiple elements
 */
function ajax_symphony_update_filter_form_fields_callback($form, $form_state) {
  $commands = array();
  $commands[] = ajax_command_replace('#sym-subclientcompany-field',
                    drupal_render($form['subinstitute_id']));
  $commands[] = ajax_command_replace('#sym-profile-types-field',
                    drupal_render($form['aoi']));

  $ajax_command = array('#type' => 'ajax', '#commands' => $commands);
    return $ajax_command;
}


/*
 * Spotlight settings
 */
function symphony_shortcode_info() {

  $shortcodes['symspotlight'] = array(
    'title' => t('Symphony Spotlight'),
    'description' => t('A spotlight on a particular event.  Can be added to any required page.'),  // Description shown along with the title in the Drupal backend
    'process callback' => 'symphony_shortcode_symspotlight'
  );

  return $shortcodes;
}


/**
 * Menu callback function to return the content for an event display page.
 */
function symphony_view_event($eventid) {
  $event = _symphony_get_event($eventid);
  if (empty($event)) {
    // No event found in symphony database, return 404.
    return MENU_NOT_FOUND;
  }

  // If we have a default ccid, return 404 if the event is not one of ours.
  $default_ccid = variable_get('symphony_default_client_company_id', '0');
  if (!empty($default_ccid) && ($event['ClientCompanyId'] != $default_ccid)) {
    return MENU_NOT_FOUND;
  }

  // Event found and valid...
  drupal_set_title($event['Title']);

  return theme('symphony_event_display_page', array('event' => $event));
}


function symphony_menu() {
  $items['events/event/%'] = array(
    'page callback' => 'symphony_view_event',
    'page arguments' => array(2),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
   );

   $items['admin/config/services/symphonyapi'] = array(
    'title' => 'Symphony API',
    'description' => 'Customisation options for the Symphony plugin',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('symphony_admin_form'),
    'access arguments' => array('administer symphonyapi settings'),
    'type' => MENU_LOCAL_TASK,
   );

   return $items;
}


/*
 * Admin options
 */
function symphony_admin_form() {
  $form = array();

  $form['symphony_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('Symphony API Key'),
    '#default_value' => variable_get('symphony_apikey', ''),
    '#description' => t("The Symphony API Key."),
    '#required' => TRUE,
  );

  $form['symphony_secretkey'] = array(
    '#type' => 'textfield',
    '#title' => t('Symphony Secret Key'),
    '#default_value' => variable_get('symphony_secretkey', ''),
    '#description' => t("The Symphony Secret Key."),
    '#required' => TRUE,
  );

	if(is_array(_symphony_get_client_company_list())){
		// Symphony Default Client Company ID.  Only check if the client company list is an array
		$form['symphony_default_client_company_id'] = array(
			'#type' => 'select',
			'#title' => t('Client company'),
			'#options' => _symphony_get_client_company_options(),
			'#default_value' => variable_get('symphony_default_client_company_id', '0'),
			'#description' => t('Select the Symphony Client company to restrict which events are displayed.'),
		);
	}

  $form['symphony_results_per_page'] = array(
    '#type' => 'select',
    '#title' => t('Number of results per page'),
    '#options' => drupal_map_assoc(range(1,10)),
    '#default_value' => variable_get('symphony_results_per_page', 10),
    '#description' => t('Number of results displayed per page in the Symphony Events block.'),
  );
  $form['symphony_num_page_links'] = array(
    '#type' => 'select',
    '#title' => t('Number of page links in the pager'),
    '#options' => drupal_map_assoc(range(1,9)),
    '#default_value' => variable_get('symphony_num_page_links', 5),
    '#description' => t('Number of numerical links displayed in the pager in the Symphony Events block.'),
  );

  $form['symphony_base_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Symphony Base path'),
    '#default_value' => variable_get('symphony_base_path', 'events'),
    '#description' => t("The primary path where the Symphony block is placed. Institute and Series links are generated relative to this path."),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

function symphony_admin_form_validate($form, &$form_state) {
  $apikey = $form_state['values']['symphony_apikey'];
  $secretkey = $form_state['values']['symphony_secretkey'];
  if (strlen($apikey)!=64) {
    form_set_error('symphony_apikey', t('The API Key is the wrong length.'));
  }
  elseif (strlen($secretkey)!=32) {
    form_set_error('symphony_secretkey', t('The secret key is the wrong length.'));
  }
}


/**
 * Internal function to return an institute link render array from a
 * a symphony event array object.
 *
 * The ClientName, ClientCompanyName and SubClientCompanyName have a specific
 * sort of interplay together. Submodules can use template preprocessing to alter
 * this field, show() it, change the link or text etc.
 * This module's tempates are configured to render the $event['institute_link'] element
 * but this function hide()s it by default.
 *
 * @param  array $event Symphony event array object
 * @return array drupal render array object representing a link
 */
function _generate_institute_link($event) {
  $inst_link = array();

  // Client name OR Client Company Name.
  $inst_text = '';
  if (!empty($event['ClientName'])) {
    $inst_text .= $event['ClientName'];

  } else if (!empty($event['ClientCompanyName'])) {
    $inst_text .= $event['ClientCompanyName'];
  }
  // The default link is to institute_id parameter
  $query = array('institute_id' => $event['ClientCompanyId']);

  // Optionally append the SubClient Company name (a 'Centre' within an institute)
  // and change the query parameters so it links to subinstitute_id.
  if (!empty($event['SubClientCompanyName'])) {
    $inst_text .= ', ' . $event['SubClientCompanyName'];
    $query = array('subinstitute_id' => $event['SubClientCompanyName']);
  }

  // Create render array
  $inst_link = array(
    '#theme' => 'link',
    '#text' => $inst_text,
    '#path' => variable_get('symphony_base_path'),
    '#options' => array('attributes' => array('target' => '_blank'),
              'html' => FALSE,
              'query' => $query),
  );
  hide($inst_link);  // Prevent display by default
  drupal_alter('institute_link', $inst_link, $event);
  return $inst_link;
}


/**
 * Internal function to return an series link render array from
 * a symphony event array object.
 * @param  array $event Symphony event array object
 * @return array drupal render array object
 */
function _generate_series_link($event) {
  $series_link = array();

  if (!empty($event['Code_Series'])) {
    // Create render array
    $series_link = array(
      '#theme' => 'link',
      '#text' => $event['Code_Series'],
      '#path' => variable_get('symphony_base_path'),
      '#options' => array('attributes' => array('target' => '_blank'),
                'html' => FALSE,
                'query' => array('series' => $event['Code_Series'])),
    );
    show($series_link);  // Display by default
    drupal_alter('series_link', $series_link, $event);
  }
  return $series_link;
}


/**
 * Internal function to return an booking for button/link render array
 * from a symphony event array object.
 * If there is an external URL we use this instead of any Symphony booking form.
 * @param  array $event Symphony event array object
 * @return array drupal render array object
 */
function _generate_booking_form_button($event) {
  $booking_button = array();
	// If an ExternalURL is set, assume the booking isn't being handled
  // by Symphony and link out to the URL given.
  if ($event['ExternalURL'] != '') {
    // Create render array
    $booking_button = array(
      '#theme' => 'link',
      '#text' => t('Book now'),
      '#path' => $event['ExternalURL'],
      '#options' => array('attributes' =>
                            array('target' => '_blank',
                                  'class' => 'button'),
                          'html' => FALSE,),
    );
  } elseif ($event['DefaultBookingFormUrl'] != '{NO DEFAULT FORM SELECTED}') {
    // Create render array
    $booking_button = array(
      '#theme' => 'link',
      '#text' => t('Book now'),
      '#path' => 'http://' . $event['DefaultBookingFormUrl'],
      '#options' => array('attributes' =>
                            array('target' => '_blank',
                                  'class' => 'button'),
                          'html' => FALSE,),
    );
  }
  return $booking_button;
}


/**
 * Internal function to return an event link render array from a
 * a symphony event array object.
 * @param  array $event Symphony event array object
 * @return array drupal render array object representing a link
 */
function _generate_event_display_link($event) {
  $event_display_link = array();
  $event_path = '/events/event/' . $event['EventId'];
  $event_display_link = array(
    '#theme' => 'link',
    '#text' => $event['Title'],
    '#path' => $event_path,
    '#options' => array('attributes' => array(),
                        'html' => FALSE,),
  );
  drupal_alter('event_display_link', $event_display_link, $event);
  return $event_display_link;
}


/**
 * Internal function to return an image render array
 * for the main image in a symphony event array object.
 * @param  array $event Symphony event array object
 * @return array drupal render array object
 */
function _generate_image_main($event) {
  $image_main = array();

  if (!empty($event['ImageLink'])) {
    // Create render array
    $event['ImageLink'] = removeAdditionalParams($event['ImageLink']);
    $image_main = array(
      '#theme' => 'image',
      '#alt' => $event['Title'],
      '#path' => $event['ImageLink'],
	  '#width' => 250,
    );
  }
  return $image_main;
}


/**
 * Internal function to return an image render array
 * for the thumbnail image in a symphony event array object.
 * @param  array $event Symphony event array object
 * @return array drupal render array object
 */
function _generate_image_thumb($event) {
  $image_main = array();

  if (!empty($event['ImageThumbLink'])) {
    // Create render array
    $event['ImageThumbLink'] = removeAdditionalParams($event['ImageThumbLink']);
    $image_main = array(
      '#theme' => 'image',
      '#alt' => $event['Title'],
      '#path' => $event['ImageThumbLink'],
    );
  }
  return $image_main;
}

/**
 * _symphony_get_client_company_list function to retrieve and cache the company list
 * @return array Client company name array, keyed by id. Empty array if error.
 */
function _symphony_get_client_company_list() {
  $ClientCompanyList = &drupal_static(__FUNCTION__);
  if (!isset($ClientCompanyList)) {
    if ($cache = cache_get('symphony_client_company_list')) {
      $ClientCompanyList = $cache->data;

    } else {
			
			try {
				$response = GetClientCompanyList();
				$ClientCompanyList = array();

				foreach ($response['GetClientCompanyListResult']['ClientCompanyResponse'] as $CompanyEntry) {
					if (isset($CompanyEntry['Name'])) {
						$ClientCompanyList[$CompanyEntry['Id']] = $CompanyEntry['Name'];
					} else {
						$ClientCompanyList[$response['GetClientCompanyListResult']['ClientCompanyResponse']['Id']] = $response['GetClientCompanyListResult']['ClientCompanyResponse']['Name'];
					}
				}
				
				cache_set('symphony_client_company_list', $ClientCompanyList, 'cache');

			} catch (SoapFault $e){
				watchdog('symphony', getFormattedOutputError($e), NULL, WATCHDOG_ERROR);
				return array();

			}
    }
  }
  return $ClientCompanyList;
}


/**
 * Returns the client company list as an '#options' array for the Form API
 */
function _symphony_get_client_company_options() {
  $all_option = array("0" => t("All"));
  $company_list = $all_option + _symphony_get_client_company_list();
  return $company_list;
}


/**
 * _symphony_get_client_company_list_by_name function to retrieve and cache the company list
 * @return array Client company id array, keyed by name.
 */
function _symphony_get_client_company_list_by_name() {
  $ClientCompanyListByName = &drupal_static(__FUNCTION__);
  if (!isset($ClientCompanyListByName)) {
    if ($cache = cache_get('symphony_client_company_list_by_name')) {
      $ClientCompanyListByName = $cache->data;

    } else {
      $ClientCompanyListByName = array();
      foreach(_symphony_get_client_company_list() as $id => $name){
        $ClientCompanyListByName[$name] = $id;
      }
      cache_set('symphony_client_company_list_by_name', $ClientCompanyListByName, 'cache');
    }
  }
  return $ClientCompanyListByName;
}


/**
 * Function to retrieve and cache the Sub-Client Company list
 * @return array Sub-client company list, keyed by company id. Empty array if error.
 */
function _symphony_get_sub_client_company_list() {
  $SubClientCompanyList = &drupal_static(__FUNCTION__);
  
  if (!isset($SubClientCompanyList)) {
    if ($cache = cache_get('symphony_get_sub_client_company_list')) {
      $SubClientCompanyList = $cache->data;

    } else {
			
			try {
				$response = GetSubClientCompanyList();
				
				$SubClientCompanyList = array();
				foreach($response['GetSubClientCompanyListResult']['GetSubClientCompanyListResponse'] as $subClient){
					//Make sure the Institute is in the array
					if(!isset($SubClientCompanyList[$subClient['ClientCompanyId']])){
						$SubClientCompanyList[$subClient['ClientCompanyId']] = array();
						$SubClientCompanyList[$subClient['ClientCompanyId']]['ClientCompanyName'] = $subClient['ClientCompanyName'];
						$SubClientCompanyList[$subClient['ClientCompanyId']]['subClients'] = array();
					}
					//Add the sub client
					$SubClientCompanyList[$subClient['ClientCompanyId']]['subClients'][] = $subClient['Name'];
				}
				
			} catch (SoapFault $e){
				watchdog('symphony', getFormattedOutputError($e), NULL, WATCHDOG_ERROR);
				return array();

			}
			
      cache_set('symphony_get_sub_client_company_list', $SubClientCompanyList, 'cache');
    }
  }
  return $SubClientCompanyList;
}


/**
 * Retrieves and transforms the sub-client company list to an '#options' array
 * (structured with option groups for each client company) for the Form API.
 * Also provides an argument to filter the options by ccid (empty or 0 for all).
 *
 * @param  array $ccids  Array of integers for selecting/limiting result to specific ccid's
 */
function _symphony_get_sub_client_company_options($ccids = array()) {
  $ccids = array_filter($ccids);  // Clean empty/null/0 values.

  $sub_clients_list = array("0" => t("All"));
  foreach (_symphony_get_sub_client_company_list() as $ccid => $value) {
    if (empty($ccids) || in_array($ccid, $ccids)) {
      $subclients = drupal_map_assoc($value['subClients']);
      $subclient = array( $value['ClientCompanyName'] => $subclients);
      $sub_clients_list += $subclient;
    }
  }

  return $sub_clients_list;
}


/**
 * Function to retrieve and cache the Profile values list
 * @return array Profile values list, keyed by company id. Empty array if error */
function _symphony_get_profile_types_list() {
  $ProfileList = &drupal_static(__FUNCTION__);
      
      
  if (!isset($ProfileList)) {
    if ($cache = cache_get('symphony_get_profile_types_list')) {
      $ProfileList = $cache->data;

    } else {
      $ProfileList = array();
      $clientcompanies = _symphony_get_client_company_list();
      
      $response = GetProfileList();
      
      $profiles = array();
      if(isset($response['GetProfileListResult']['ProfileTypes']['ProfileList']['ClientCompanyId'])){
				$profiles[] = $response['GetProfileListResult']['ProfileTypes']['ProfileList'];
			}else{
				$profiles = $response['GetProfileListResult']['ProfileTypes']['ProfileList'];
			}
      
      foreach ($profiles as $index => &$profile) {
        // Add Client company name
        if (!empty($clientcompanies[$profile['ClientCompanyId']])) {
          $profile['ClientCompanyName'] = $clientcompanies[$profile['ClientCompanyId']];
        }

        // Reform the ProfileValues into key=>value array.
        $new_prof_vals = array();
        foreach ($profile['ProfileValues']['ProfileValue'] as $key => $value) {
          $new_prof_vals += array($value['Id'] => $value['Name']);
        }
        $profile['ProfileValues'] = $new_prof_vals;

        // Push with ccid as key (makes structure consistent with other lists).
        $ProfileList += array($profile['ClientCompanyId'] => $profile);
      }
      
      cache_set('symphony_get_profile_types_list', $ProfileList, 'cache');
    }
  }

  return $ProfileList;
}


/**
 * Retrieves and transforms the Profile types list to an '#options' array
 * (structured with option groups for each client company) for the Form API.
 * Also provides an argument to filter the options by ccid (empty or 0 for all).
 *
 * @param  array $ccids  Array of integers for selecting/limiting result to specific ccid's
 */
function _symphony_get_profile_types_options($ccids = array()) {
  $ccids = array_filter($ccids);  // Clean empty/null/0 values.

  $options = array("0" => t("Any"));
  $profiletypes = _symphony_get_profile_types_list();

  // Put SAS Central at the top (ccid == 343) outside of any option group.
  if (!empty($profiletypes['343'])) {
    $options += $profiletypes['343']['ProfileValues'];
    unset($profiletypes['343']);
  }

  // Add rest of option groups
  foreach ($profiletypes as $ccid => $value) {
    if (empty($ccids) || in_array($ccid, $ccids)) {
      $options += array($value['ClientCompanyName'] => $value['ProfileValues']);
    }
  }

  return $options;
}


/**
 * Retrieve and event from Symphony API, preprocess and return an event array
 * @param  mixed $eventid symphony event id
 * @return array or NULL Event object or NULL if event id not found.
 */
function _symphony_get_event($eventid) {
  $eventid = intval($eventid);
  
  try {
    $response = GetEventDetails2($eventid);
    $event = array();
		
		if(isset($response['GetEventDetails2Result']['EventId'])){
			$event = $response['GetEventDetails2Result'];
			
			// Add the event id because it is not currently added in getEventDetails2()
			$event['ClientCompanyId'] = _symphony_get_client_company_list_by_name()[$event['ClientCompanyName']];

			// Prepare the event and institute link render array for the event
			$event['event_display_link'] = _generate_event_display_link($event);
			$event['institute_link'] = _generate_institute_link($event);
			$event['series_link'] = _generate_series_link($event);
			$event['booking_form_button'] = _generate_booking_form_button($event);
			$event['image_main'] = _generate_image_main($event);
			$event['image_thumb'] = _generate_image_thumb($event);

			// If event has a symphony website page template, load that into the event array
			// and the template will use it instead of the specific fields.
			if($event['DefaultWebsiteID']!=0){
				$event['WebsitePageContent'] = _symphony_get_website_page_content($event['DefaultWebsiteID'], $event['DefaultWebsitePageFriendlyName']);
			}

			$event['Description'] = check_plain($event['Description']);

			// Event results description text
			$resultDesc = $event['Description'];
			if(strlen($event['Description'])>RESULT_DESC_CHARS){
				$resultDesc = substr($event['Description'], 0, RESULT_DESC_CHARS-3) . "...";
			}
			$event['ResultDescription'] = check_markup($resultDesc);

			// Event spotlight description text
			$spotlightDesc = $event['Description'];
			if(strlen($event['Description'])>SPOTLIGHT_DESC_CHARS){
				$spotlightDesc = substr($event['Description'], 0, SPOTLIGHT_DESC_CHARS-3) . "...";
			}
			$event['SpotlightDescription'] = check_markup($spotlightDesc);
			
    }

  } catch (SoapFault $e){
    watchdog('symphony', getFormattedOutputError($e), NULL, WATCHDOG_ERROR);
    return array();

  }

  return $event;
  

}


/**
 * Retrieve events from Symphony API, preprocess and return a results array
 * This is essentially a wrapper over the GetExtEventsDetailsX() Symphony API function
 * so we pass the parameters through to that API call, recieve the response,
 * process the response/events a bit and return the array to the caller.
 */
function _symphony_get_results($instituteid, $start_date, $end_date, $event_type, $event_name, $subclient_company, $page, $results_per_page, $profileSearchArray, $series) {

  // Check for empty (or equvalent) params and ensure they are NULLed for the SOAP request
  if (($instituteid == 0) || ($instituteid == 343)) {
    // SOAP request needs NULL for the SAS Company id or All
    $instituteid = NULL;
  }
  
  try {
		$response = GetExtEventsDetails3($instituteid, $start_date, $end_date, $event_type, $event_name, $subclient_company, $page, $results_per_page, $profileSearchArray, $series);

		$client_company_list = _symphony_get_client_company_list_by_name();
		    
    $results = $response['GetExtEventsDetails3Result'];
    
    $events = array();
    $events['numresults'] = $results['TotalEventCount'];
    
    if(isset($results['Events']['EventExtendedDetailsResponse3Details']['EventId'])){
			$events['events'][] = $results['Events']['EventExtendedDetailsResponse3Details'];
			
		}else{
			foreach($results['Events']['EventExtendedDetailsResponse3Details'] as $event){
				$events['events'][] = $event;
			}
		}
    
    // Preprocess the SOAP response array before passing to the template
		foreach ($events['events'] as $eventid => &$event) {
			$resultDesc = $event['Description'];
			if(strlen($event['Description'])>RESULT_DESC_CHARS){
				$resultDesc = substr($event['Description'], 0, RESULT_DESC_CHARS-3) . "...";
			}
			$event['ResultDescription'] = check_markup($resultDesc);
			$event['ClientCompanyId'] = $client_company_list[$event['ClientCompanyName']];
      $event['StartDateUnix'] = strtotime($event['StartDate']);
      $event['EndDateUnix'] = strtotime($event['EndDate']);

			// Prepare the event and institute link render array for the event
			$event['event_display_link'] = _generate_event_display_link($event);
			$event['institute_link'] = _generate_institute_link($event);
			$event['series_link'] = _generate_series_link($event);
			$event['booking_form_button'] = _generate_booking_form_button($event);
			$event['image_main'] = _generate_image_main($event);
			$event['image_thumb'] = _generate_image_thumb($event);
		}
		
  } catch (SoapFault $e){
    watchdog('symphony', getFormattedOutputError($e), NULL, WATCHDOG_ERROR);
    return array();

  }

  return $events;

}

function _symphony_get_website_page_content($DefaultWebsiteID, $DefaultWebsitePageFriendlyName){
	try {
    $response = GetWebsitePageContent($DefaultWebsiteID, $DefaultWebsitePageFriendlyName);
    $content = "";

    $content = $response['GetWebsitePageContentResult'];

  } catch (SoapFault $e){
    watchdog('symphony', getFormattedOutputError($e), NULL, WATCHDOG_ERROR);
    return array();

  }

  return $content;
}

function _symphony_get_event_type_list(){
  try {
    $response = GetEventTypeList();
    $types = array();

		foreach ($response['GetEventTypeListResult']['EventType']['string'] as $type) {
			$types[$type] = $type;
		}

  } catch (SoapFault $e){
    watchdog('symphony', getFormattedOutputError($e), NULL, WATCHDOG_ERROR);
    return array();

  }

  return $types;
}

/**
 * Get speakers for event wrapper over Symphony PHP SOAP layer
 * which logs exceptions and simplifies the response values.
 */
function _symphony_get_speakers_for_event($eventid){
  try {
    $response = GetSpeakersforEvent($eventid);
    $speakers = array();

    // Unfortunately the Symphony response data structure is different
    // if num speakers == 1 or if > 1.
    // We need to test for the first case by checking for 'SpeakerId' key.
    foreach ($response['GetSpeakersforEventResult'] as $speaker) {
      if (isset($speaker['SpeakerId'])) {
        $speakers[] = $speaker;
      } else {
        $speakers = $speaker;
      }
    }

    // Prepare common formatted string values to use in templates
    // {Forename} {Surname} ({Organisation})
    // Templates can override and render their own custom formatted speaker values.
    foreach ($speakers as &$speaker) {
      $speaker_str = trim(format_string('@first @last', array(
        '@first' => $speaker['Forename'],
        '@last' => $speaker['Surname']
      )));
      if (!empty($speaker['Organisation'])) {
        $speaker_str .= format_string(' (@org)', array('@org' => $speaker['Organisation']));
      }
      $speaker['speaker_formatted'] = $speaker_str;
    }

  } catch (SoapFault $e){
    watchdog('symphony', getFormattedOutputError($e), NULL, WATCHDOG_ERROR);
    return array();

  }

  return $speakers;
}
